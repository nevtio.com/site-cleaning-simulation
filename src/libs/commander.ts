import colors from 'colors'
import { App } from '~/libs/app'

export const execCommand = async (app: App, command: string): Promise<true | number> => {
    try {
        if (command.match(/(\bl\b)|\b(left)\b/)) {
            app.turnLeft() // Left
            await app.performNextCommandPending()
        } else if (command.match(/(\br\b)|\b(right)\b/)) {
            app.turnRight() // Right
            await app.performNextCommandPending()
        } else if (command.match(/(\ba (\d+)\b)|\b(advance (\d+))\b/)) {
            const step = +(command.match(/. (\d+)/)?.[1] ?? 1) // Advance (Step)
            app.advance(step)
            await app.performNextCommandPending()
        } else if (command.match(/(\bq\b)|\b(quit)\b/)) {
            return 0
        } else {
            console.error(colors.red(`Unknown command ("${command}").`)) // Unknown command
        }
    } catch (error) {
        console.error(colors.red(error instanceof Error ? error.message : '' + error)) // Display error and terminate the program
        return 1
    }
    return true
}
