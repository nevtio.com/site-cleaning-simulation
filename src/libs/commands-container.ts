import { Command, Expense } from '~/types'

export class CommandContainer {
    protected _pendings: Command[]
    protected _performs: Array<{ command: Command; expenses: Expense[] }>

    constructor() {
        this._pendings = []
        this._performs = []
    }

    get pendings(): Command[] {
        return this._pendings.map((command: Command) => {
            return { ...command }
        })
    }

    get performs(): Array<{ command: Command; expenses: Expense[] }> {
        return this._performs.map((processed: { command: Command; expenses: Expense[] }) => {
            return { ...processed }
        })
    }

    pushCommand(command: Command): CommandContainer {
        this._pendings.push(command)
        return this
    }
    stepCommand(): Command | undefined {
        return this._pendings.shift()
    }
    comleteCommand(command: Command, expenses: Expense[]): CommandContainer {
        this._performs.push({ command, expenses })
        return this
    }
}
