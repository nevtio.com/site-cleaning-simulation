/*
    Helper functions for calculations
*/

import costsData from 'data/costs.json'
import fuelsData from 'data/fuels.json'
import { Site } from '~/libs/site'
import { BlockType, Command, CostType, Expense, FuelType, ReportItem } from '~/types'

export const defaultFuel = 0
export const defaultCost = 0

export const getFuel = (fuelType: FuelType, qty = 1): number => {
    return (fuelsData[fuelType] ?? defaultFuel) * qty
}

export const getCost = (costType: CostType, qty: number): number => {
    return (costsData[costType] ?? defaultCost) * qty
}

export const calculateStepExpense = (blockType: BlockType, params: { isStoppingOnBlock: boolean }): Expense => {
    if (blockType === BlockType.PLAIN)
        // Plain
        return {
            fuelType: params.isStoppingOnBlock ? FuelType.CLEAR_PLAIN_LAND : FuelType.VISIT_PLAIN_LAND,
            costType: CostType.NONE,
        }
    else if (blockType === BlockType.ROCKY)
        // Rocky
        return {
            fuelType: params.isStoppingOnBlock ? FuelType.CLEAR_ROCKY_LAND : FuelType.VISIT_ROCKY_LAND,
            costType: CostType.NONE,
        }
    else if (blockType === BlockType.TREE)
        // Tree
        return {
            fuelType: params.isStoppingOnBlock ? FuelType.CLEAR_TREE_LAND : FuelType.VISIT_TREE_LAND,
            costType: params.isStoppingOnBlock ? CostType.NONE : CostType.REPAIR_BULLDOZER,
        }
    else if (blockType === BlockType.PRESERVED_TREE)
        // Preserved Tree
        return {
            fuelType: FuelType.NONE,
            costType: CostType.DESTRUCTION_PRESERVED_TREE,
        }

    return { fuelType: FuelType.NONE, costType: CostType.NONE }
}

export const generateReportItems = (site: Site, performedCommands: Array<{ command: Command; expenses: Expense[] }>): ReportItem[] => {
    // Items
    const communication = { item: 'communication', qty: 0, cost: 0 }
    const overheadFuelUsage = { item: 'overhead fuel usage', qty: 0, cost: 0 }
    const unclearedSquares = { item: 'uncleared squares', qty: 0, cost: 0 }
    const destructionOfProtectedTree = { item: 'destruction of protected tree', qty: 0, cost: 0 }
    const paintDamageToBulldozer = { item: 'paint damage to bulldozer', qty: 0, cost: 0 }

    // Perform loop on expenses
    performedCommands.forEach(({ expenses }: { command: Command; expenses: Expense[] }) => {
        expenses.forEach((expense: Expense) => {
            if (expense.costType === CostType.COMMUNICATION) communication.qty++
            else if (expense.costType === CostType.REPAIR_BULLDOZER) paintDamageToBulldozer.qty++
            else if (expense.costType === CostType.DESTRUCTION_PRESERVED_TREE) destructionOfProtectedTree.qty++
            if (expense.fuelType !== FuelType.NONE) overheadFuelUsage.qty += getFuel(expense.fuelType)
        })
    })

    // Check uncleared blocks
    unclearedSquares.qty = (site.toString().match(/[^oT\s]/g) || []).length

    // Sum the costs
    communication.cost = getCost(CostType.COMMUNICATION, communication.qty)
    overheadFuelUsage.cost = getCost(CostType.FUEL, overheadFuelUsage.qty)
    unclearedSquares.cost = getCost(CostType.UNCLEAR, unclearedSquares.qty)
    destructionOfProtectedTree.cost = getCost(CostType.DESTRUCTION_PRESERVED_TREE, destructionOfProtectedTree.qty)
    paintDamageToBulldozer.cost = getCost(CostType.REPAIR_BULLDOZER, paintDamageToBulldozer.qty)

    return [communication, overheadFuelUsage, unclearedSquares, destructionOfProtectedTree, paintDamageToBulldozer]
}
