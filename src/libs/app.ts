import { Bulldozer } from '~/libs//bulldozer'
import { CommandContainer } from '~/libs/commands-container'
import { Site } from '~/libs/site'
import { Command, CommandType, CostType, Expense, Facing, FuelType, ReportItem } from '~/types'
import { calculateStepExpense } from './calculator'
import EventEmitter from 'events'

export class App extends EventEmitter {
    protected _bulldozer: Bulldozer
    protected _site: Site
    protected _commandContainer: CommandContainer

    constructor(siteData: string) {
        super()
        this._site = new Site()
        this._bulldozer = new Bulldozer(this._site, Facing.EAST)
        this._commandContainer = new CommandContainer()
        this._site.load(siteData)
    }

    get site(): Site {
        return this._site
    }
    get bulldozer(): Bulldozer {
        return this._bulldozer
    }
    get pendingCommands(): Command[] {
        return this._commandContainer.pendings
    }
    get performedCommands(): Array<{ command: Command; expenses: Expense[] }> {
        return this._commandContainer.performs
    }

    turnLeft(): App {
        this.addCommand({ type: CommandType.LEFT })
        return this
    }
    turnRight(): App {
        this.addCommand({ type: CommandType.RIGHT })
        return this
    }
    advance(step: number): App {
        this.addCommand({ type: CommandType.ADVANCE, payload: { step } })
        return this
    }
    addCommand(command: Command): App {
        this._commandContainer.pushCommand(command)
        this.emit('command-pushed', { command })
        return this
    }
    terminate(reportItems: ReportItem[]): App {
        this.emit('terminate', reportItems)
        return this
    }
    async performNextCommandPending(): Promise<void> {
        const command = this._commandContainer.stepCommand()
        if (!command) throw new Error('No further command is pending.')
        const expenses = await this._execCommand(command)
        this._commandContainer.comleteCommand(command, expenses)
        this.emit('command-executed', { command, expenses })

        // Check if any attempt on preserved tree
        if (expenses.find(({ costType }: { costType: CostType }) => costType === CostType.DESTRUCTION_PRESERVED_TREE))
            throw new Error('Attempt to visit or pass through preserved tree.')
    }

    private async _execCommand(command: Command): Promise<Expense[]> {
        switch (command.type) {
            case CommandType.LEFT: {
                this.bulldozer.rotate(-1)
                return [{ fuelType: FuelType.NONE, costType: CostType.COMMUNICATION }]
            }

            case CommandType.RIGHT: {
                this.bulldozer.rotate(1)
                return [{ fuelType: FuelType.NONE, costType: CostType.COMMUNICATION }]
            }

            case CommandType.ADVANCE: {
                const expenses = [{ fuelType: FuelType.NONE, costType: CostType.COMMUNICATION }]
                const steps = command.payload?.step ?? 1
                for (let i = 0; i < steps; i++) {
                    // Run each step
                    this.bulldozer.move()
                    const blockType = this.site.getBlockType(this.bulldozer.position)
                    if (blockType) {
                        const expense = calculateStepExpense(blockType, { isStoppingOnBlock: i === steps - 1 })
                        expenses.push(expense)
                        if (expense.costType === CostType.DESTRUCTION_PRESERVED_TREE) break
                    }
                }
                if (steps > 0) this.site.clearBlock(this.bulldozer.position)
                return expenses
            }
        }
        throw new Error(`Unknown command type "${command.type}".`)
    }
}
