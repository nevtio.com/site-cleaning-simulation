import colors from 'colors'
import readline from 'readline'
import { App } from '~/libs/app'
import { execCommand } from '~/libs/commander'
import { end } from '~/libs/modules/end'

// Interactive system
export const runInteractive = (app: App) => {
    console.log(colors.yellow('Welcome to the Aconex site clearing simulator. This is a map of the site:'))
    console.log(app.site.toString())
    console.log(colors.yellow('The bulldozer is currently located at the Northern edge of the site, immediately to the West of the site, and facing East.'))

    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false,
    })
    // Command loop
    const loop = () => {
        rl.question('(l)eft, (r)ight, (a)dvance <n>, (q)uit:\r\n', async (ans: string) => {
            const res = await execCommand(app, ans)
            if (res === true) {
                loop()
            } else {
                rl.close()
                app.terminate(end(app, res !== 0))
                setTimeout(() => {
                    process.exit(res)
                }, 100)
            }
        })
    }

    // Star the loop in the next tick
    setTimeout(loop, 1)
}
