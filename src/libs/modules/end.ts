import colors from 'colors'
import { printTable } from 'console-table-printer'
import { App } from '~/libs/app'
import { generateReportItems } from '~/libs/calculator'
import { CommandType, ReportItem } from '~/types'

// Ending report
export const end = (app: App, hasError = false): ReportItem[] => {
    console.log(colors.yellow(`The simulation has ended ${hasError ? 'due to error' : 'at your request'}. These are the commands you issued:`))
    const performedCommandLabels = app.performedCommands
        .map(({ command }) => {
            switch (command.type) {
                case CommandType.LEFT:
                    return 'turn left'
                case CommandType.RIGHT:
                    return 'turn right'
                case CommandType.ADVANCE:
                    return `advance ${command.payload?.step ?? 1}`
            }
            return 'unknown'
        })
        .join(', ')
    console.log(colors.green(performedCommandLabels))
    console.log(colors.yellow('The costs for this land clearing operation were:'))
    // app.performedCommands.forEach((c) => console.log(c))
    // console.log(app.site.toString())

    const reportItems = generateReportItems(app.site, app.performedCommands)
    printTable([...reportItems, {}, { item: 'total', cost: reportItems.reduce((sum: number, reportItem: ReportItem) => sum + reportItem.cost, 0) }])

    return reportItems
}
