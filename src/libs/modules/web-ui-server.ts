import colors from 'colors'
import cors from 'cors'
import express from 'express'
import httpServer from 'http'
import path from 'path'
import serveStatic from 'serve-static'
import { Server as SocketIOServer, Socket } from 'socket.io'
import { App } from '~/libs/app'
import { execCommand } from '~/libs/commander'
import { BlockType, Command, Expense, Facing, Position, ReportItem } from '~/types'
import { end } from '~/libs/modules/end'

// Web UI
export const runWebUiServer = (app: App, port: number) => {
    const webApp = express()
    const server = httpServer.createServer(webApp)
    const socketServer = new SocketIOServer(server, {
        cors: {},
    })

    webApp.use(cors())
    webApp.use(serveStatic(path.join(process.cwd(), './public'))) // Static public resources

    // Socket connections
    socketServer.on('connection', (socket: Socket) => {
        console.log(colors.blue(`Socket "${socket.id}" connected."`))
        socket.on('command', async ({ command }: { command: string }, cb: (res: true | number) => void) => {
            console.log(colors.blue(`Command "${command}" received from socket "${socket.id}"."`))
            const res = await execCommand(app, command)
            if (res === true) {
                cb(res)
            } else {
                app.terminate(end(app, res !== 0))
                setTimeout(() => {
                    process.exit(res)
                }, 100)
            }
        })
        socket.on('get-site-data', async (_params: any, cb: ({ blocks, bulldozer }: { blocks: BlockType[][]; bulldozer: { position: Position; facing: Facing } }) => void) => {
            const blocks = app.site.blocks
            const bulldozer = { position: app.bulldozer.position, facing: app.bulldozer.facing }
            cb({ blocks, bulldozer })
        })
        socket.on('list-performed-commands', async (_params: any, cb: (performedCommands: Array<{ command: Command; expenses: Expense[] }>) => void) => {
            cb(app.performedCommands)
        })
        socket.on('disconnect', () => {
            console.log(colors.blue(`Socket "${socket.id}" disconnected."`))
        })
    })

    server.listen(port, (error?: Error) => {
        if (error) throw error
        console.log(colors.blue(`Web UI Server is listening on port ${port}...`))
    })

    // Watch app events
    app.addListener('command-executed', ({ command, expenses }: { command: Command; expenses: Expense[] }) => {
        socketServer.emit('command-executed', { command, expenses })
    })
    app.addListener('terminate', (reportItems: ReportItem[]) => {
        socketServer.emit('terminate', reportItems)
    })
}
