import { Site } from '~/libs/site'
import { Facing, Position } from '~/types'

const TotalFaces = Object.keys(Facing).length / 2

export class Bulldozer {
    protected _facing: Facing
    protected _position: Position

    constructor(private readonly _site: Site, facing: Facing) {
        this._facing = facing
        this._position = { x: -1, y: 0 }
    }

    get facing(): Facing {
        return this._facing
    }
    get position(): Position {
        return { ...this._position }
    }

    setPosition(position: Position): Bulldozer {
        this._position.x = position.x
        this._position.y = position.y
        return this
    }
    rotate(turn: number): Bulldozer {
        this._facing = (this._facing + turn) % TotalFaces
        this._facing = this._facing < 0 ? TotalFaces + this._facing : this._facing
        return this
    }

    move(): Bulldozer {
        switch (this._facing) {
            case Facing.NORTH:
                if (this._position.y - 1 < 0)
                    throw new Error(`Unable to advance to the north, the new position will be out of range (x:${this._position.x}, y:${this._position.y - 1})`)
                this._position.y--
                break
            case Facing.EAST:
                if (this._position.x + 1 >= this._site.width)
                    throw new Error(`Unable to advance to the east, the new position will be out of range (x:${this._position.x + 1}, y:${this._position.y})`)
                this._position.x++
                break
            case Facing.SOUTH:
                if (this._position.y + 1 >= this._site.height)
                    throw new Error(`Unable to advance to the south, the new position will be out of range (x:${this._position.x}, y:${this._position.y + 1})`)
                this._position.y++
                break
            case Facing.WEST:
                if (this._position.x - 1 < 0)
                    throw new Error(`Unable to advance to the west, the new position will be out of range (x:${this._position.x - 1}, y:${this._position.y})`)
                this._position.x--
                break
        }
        return this
    }
}
