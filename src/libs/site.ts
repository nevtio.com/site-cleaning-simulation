import { BlockType, Position } from '~/types'

export class Site {
    protected _blocks: BlockType[][]

    constructor(width = 5, height = 5, value?: BlockType) {
        this._blocks = []
        this.reset(value, width, height)
    }

    get height(): number {
        return this._blocks.length
    }
    get width(): number {
        return this._blocks.length > 0 ? this._blocks[0].length : 0
    }
    get blocks(): BlockType[][] {
        return this._blocks.map((rows: BlockType[]) => {
            return [...rows]
        })
    }

    getBlockType(position: Position): BlockType | undefined {
        return this._blocks[position.y][position.x]
    }

    clearBlock(position: Position): Site {
        if (this._blocks[position.y][position.x]) this._blocks[position.y][position.x] = BlockType.PLAIN
        return this
    }

    reset(value?: BlockType, width?: number, height?: number): Site {
        width = width ?? this.width
        height = height ?? this.height
        value = value ?? BlockType.PLAIN
        this._blocks = Array(height)
            .fill([])
            .map(() => Array(width).fill(value))
        return this
    }
    load(siteMapData: string): Site {
        const rows = siteMapData.split(/\s/)
        const height = rows.length
        if (rows.length > 0) {
            const width = rows[0].length
            this.reset(BlockType.PLAIN, width, height)
            rows.forEach((row: string, y: number) => {
                if (row.length === width)
                    row.split('').forEach((blockData: string, x: number) => {
                        if (Object.values(BlockType).includes(blockData as BlockType)) this._blocks[y][x] = blockData as BlockType
                    })
            })
        }
        return this
    }

    toString(): string {
        return this._blocks
            .map((row: string[]) => {
                return row.join('')
            })
            .join(' ')
    }
}
