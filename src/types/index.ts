export enum CommandType {
    LEFT = 1,
    RIGHT,
    ADVANCE,
    QUIT,
}

export interface Command {
    type: CommandType
    payload?: {
        step?: number
    }
}

export enum BlockType {
    PLAIN = 'o',
    ROCKY = 'r',
    TREE = 't',
    PRESERVED_TREE = 'T',
}

export enum Facing {
    NORTH,
    EAST,
    SOUTH,
    WEST,
}

export enum FuelType {
    VISIT_PLAIN_LAND = 'visitPlainLand',
    CLEAR_PLAIN_LAND = 'clearPlainLand',
    VISIT_ROCKY_LAND = 'visitRockyLand',
    CLEAR_ROCKY_LAND = 'clearRockyLand',
    VISIT_TREE_LAND = 'visitTreeLand',
    CLEAR_TREE_LAND = 'clearTreeLand',
    NONE = 'none',
}

export enum CostType {
    COMMUNICATION = 'communication',
    FUEL = 'fuel',
    UNCLEAR = 'unclear',
    DESTRUCTION_PRESERVED_TREE = 'destructionPreservedTree',
    REPAIR_BULLDOZER = 'repairBulldozer',
    NONE = 'none',
}

export interface Position {
    x: number
    y: number
}

export interface ReportItem {
    item: string
    qty: number
    cost: number
}

export enum CommandExecResultStatus {
    OK = 0,
    ERROR,
}

export interface Expense {
    fuelType: FuelType
    costType: CostType
}
