import colors from 'colors'
import { Command } from 'commander'
import fs from 'fs'
import path from 'path'
import { App } from '~/libs/app'
import { runInteractive } from './libs/modules/interactive'
import { runWebUiServer } from './libs/modules/web-ui-server'

// Entrypoint
const program = new Command()
program
    .name('site-cleaning-simulation')
    .version('1.0.0', '-v, --version', 'output the current version')
    .description('Site Cleaning Simulation')
    // .option('-m, --map [map-file]', 'Load site map from a static file.')
    .option('-w, --web <port>', 'Run Web UI server with port number.')
    .argument('<site-map-data-path>', 'Site map data to be loaded.')
    .configureOutput({
        writeOut: (str) => console.log(colors.yellow(str)),
        outputError: (str, write) => write(colors.red(str)),
    })
    .showHelpAfterError(colors.yellow(program.helpInformation()))
    .action((siteMapDataPath, options) => {
        // 1. Load the map data from given map data path
        const siteMapData = (() => {
            try {
                return fs.readFileSync(path.resolve(process.cwd(), siteMapDataPath)).toString()
            } catch (error) {
                console.error(colors.red(`Unable to load the site map data from the path ${siteMapDataPath} => ${error instanceof Error ? error.message : ''}`))
                process.exit(1)
            }
        })()

        // 2. Start the app and load the site map data
        const app = new App(siteMapData)

        // 3. Run the interactive system
        runInteractive(app)

        // 4. Run web UI server (optional)
        if (options.web) runWebUiServer(app, !isNaN(options.web) ? +options.web : 8080)
    })

program.parse(process.argv)
