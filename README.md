# Site Cleaning Simulation

Oracle - Aconex site clearing simulator.

The source code could be access from GitLab
https://gitlab.com/nevtio.com/site-cleaning-simulation

## The Tech Stack

-   Application (Node.js)

    -   Typescript / ES6
    -   JEST (Testing framework)
    -   <s>Snowpack (Build tool) https://www.snowpack.dev/</s>
    -   Webpack (Build tool and bundler) https://webpack.js.org/
    -   pkg (Compiler / Package bundler) https://github.com/vercel/pkg#readme

-   Web UI Server
    -   WebUI - React / React Bootstrap (Framework and UI components library) https://react-bootstrap.github.io/ and SCSS (CSS pre-processors)
    -   Socket.IO (Realtime Server/Client communication) https://socket.io/

## Executables

-   There are built and compiled executables available from the CI/CD pipeline, please refer to the release page for further details https://gitlab.com/nevtio.com/site-cleaning-simulation/-/releases

-   Otherwise, you may git clone the source codes and build / compile the executable with the following commands. Prerequisite: `Node.js Runtime` (http://www.nodejs.org), `YARN` (http://www.yarnpkg.com).

    ```
    $ git clone https://gitlab.com/nevtio.com/site-cleaning-simulation.git
    $ cd site-cleaning-simulation
    $ yarn install
    $ yarn build
    ```

## Run

-   To run the application under Node.js (12+) runtime, you could do the following:

    ```
    $ node dist/index.js <site-map-data>
    ```

    Example shown as following:

    ```
    $ node dist/index.js ./samples/test.data

    Welcome to the Aconex site clearing simulator. This is a map of the site:
    ootooooooo oooooooToo rrrooooToo rrrroooooo rrrrrtoooo
    The bulldozer is currently located at the Northern edge of the site, immediately to the West of the site, and facing East.
    (l)eft, (r)ight, (a)dvance <n>, (q)uit:
    r
    (l)eft, (r)ight, (a)dvance <n>, (q)uit:
    a 3
    (l)eft, (r)ight, (a)dvance <n>, (q)uit:
    l
    (l)eft, (r)ight, (a)dvance <n>, (q)uit:
    a 5
    (l)eft, (r)ight, (a)dvance <n>, (q)uit:
    r
    (l)eft, (r)ight, (a)dvance <n>, (q)uit:
    q
    The simulation has ended at your request. These are the commands you issued:
    turn right, advance 3, turn left, advance 5, turn right
    The costs for this land clearing operation were:
    ┌───────────────────────────────┬─────┬──────┐
    │                          item │ qty │ cost │
    ├───────────────────────────────┼─────┼──────┤
    │                 communication │   5 │    5 │
    │           overhead fuel usage │   5 │    5 │
    │             uncleared squares │  14 │   42 │
    │ destruction of protected tree │   0 │    0 │
    │     paint damage to bulldozer │   0 │    0 │
    │                               │     │      │
    │                         total │     │   52 │
    └───────────────────────────────┴─────┴──────┘
    ```

-   Or you could checkout the help documment by passing `--help` parameters

    ```
    $ node /dist/index.js --help
    Usage: site-cleaning-simulation [options] <site-data-path>

    Site Cleaning Simulation

    Arguments:
    site-data-path    Site map data to be loaded.

    Options:
    -v, --version     output the current version
    -w, --web <port>  Run Web UI server with port number.
    -h, --help        display help for command
    ```

## Run with Web UI Server

-   Optionally, you could run the application with Web UI Server support.

    ```
    $ node dist/index.js -w [port] <site-map-data>
    ```

    Example shown as following:

    ```
    $ node dist/index.js -w 3000 ./samples/test.data

    Welcome to the Aconex site clearing simulator. This is a map of the site:
    ootooooooo oooooooToo rrrooooToo rrrroooooo rrrrrtoooo
    The bulldozer is currently located at the Northern edge of the site, immediately to the West of the site, and facing East.
    Web UI Server is listening on port 3000...
    (l)eft, (r)ight, (a)dvance <n>, (q)uit:
    ```

    Once the application started, you could either instruct the bulldozer with Command Line Interface (CLI) controller, or with Web UI Controller.

    Open a browser and navigate to URL

    ```
    http://<your-ip>:3000
    or
    http://localhost:3000 (If you running from the local computer)
    ```

## ToDo

Following features are currently in pending:

-   Batch runner (Import / Export) - To import the commands instruction and export the result as JSON data
-   <s>Web UI Server - The nicer UI display and controller as web application, and to allow the controller to send commands as REST API to instruct the simulator.</s>
-   Unit / Integration Test
