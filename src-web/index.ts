import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import './styles/app.scss'
import './services/socket'
ReactDOM.render(React.createElement(App), document.getElementById('app'))
