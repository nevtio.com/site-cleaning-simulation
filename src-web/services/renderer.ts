import { BlockType, Facing, Position } from '../types'

const blockSize = 50
const rotations = [270, 0, 90, 180]

const drawBulldozer = (ctx: CanvasRenderingContext2D, position: Position, facing: Facing): void => {
    const _x = position.x * blockSize
    const _y = position.y * blockSize

    // Draw body
    ctx.beginPath()
    ctx.fillStyle = '#e5f048'
    ctx.arc(_x + blockSize / 2, _y + blockSize / 2, blockSize * 0.35, 0, 2 * Math.PI)
    ctx.fill()
    ctx.closePath()

    // Draw facing
    ctx.save()
    ctx.translate(_x + blockSize / 2, _y + blockSize / 2)
    ctx.rotate((rotations[facing] * Math.PI) / 180)
    ctx.beginPath()
    ctx.fillStyle = '#e69f43'
    ctx.moveTo(-blockSize * 0.125, -blockSize * 0.25)
    ctx.lineTo(-blockSize * 0.125, blockSize * 0.25)
    ctx.lineTo(blockSize * 0.35, 0)
    ctx.fill()
    ctx.restore()
    return
}
const drawCrosshair = (ctx: CanvasRenderingContext2D, position: Position, facing: Facing, step: number): void => {
    const _x = (position.x + (facing === Facing.EAST ? step : facing === Facing.WEST ? -step : 0)) * blockSize
    const _y = (position.y + (facing === Facing.SOUTH ? step : facing === Facing.NORTH ? -step : 0)) * blockSize

    ctx.save()
    ctx.strokeStyle = '#ff0000'
    ctx.lineWidth = Math.max(blockSize * 0.1, 5)
    ctx.translate(_x, _y)
    ctx.strokeRect(ctx.lineWidth / 2, ctx.lineWidth / 2, blockSize - ctx.lineWidth, blockSize - ctx.lineWidth)
    ctx.restore()
    return
}
const drawPlainBlock = (ctx: CanvasRenderingContext2D, position: Position): void => {
    return drawBlock(ctx, position, '#8f997c', '')
}
const drawRockyBlock = (ctx: CanvasRenderingContext2D, position: Position): void => {
    return drawBlock(ctx, position, '#826b46', 'R')
}
const drawTreeBlock = (ctx: CanvasRenderingContext2D, position: Position): void => {
    return drawBlock(ctx, position, '#74ad49', 'T')
}
const drawPreservedTreeBlock = (ctx: CanvasRenderingContext2D, position: Position): void => {
    return drawBlock(ctx, position, '#ad583e', 'P')
}

const drawBlock = (ctx: CanvasRenderingContext2D, position: Position, color: string, text: string): void => {
    const _x = position.x * blockSize
    const _y = position.y * blockSize
    ctx.beginPath()
    ctx.fillStyle = color
    ctx.rect(_x, _y, blockSize, blockSize)
    ctx.fill()
    ctx.font = '16px'
    ctx.fillStyle = '#333333'
    ctx.fillText(text[0] ?? '', _x + blockSize / 2 - 2, _y + blockSize / 2 + 4)
    return
}

export const render = (canvasEl: HTMLCanvasElement, blocks: BlockType[][], bulldozer: { position: Position; facing: Facing }, step: number) => {
    const ctx = canvasEl.getContext('2d')
    if (ctx) {
        // Width / Height
        const numY = blocks.length
        const numX = numY > 0 ? blocks[0].length : 0

        // Fix the size
        canvasEl.width = blockSize * numX
        canvasEl.height = blockSize * numY

        // Draw each block
        ctx.clearRect(0, 0, canvasEl.width, canvasEl.height)
        blocks.forEach((row: BlockType[], y: number) => {
            row.forEach((block: BlockType, x: number) => {
                if (block === BlockType.PLAIN) drawPlainBlock(ctx, { x, y })
                else if (block === BlockType.ROCKY) drawRockyBlock(ctx, { x, y })
                else if (block === BlockType.TREE) drawTreeBlock(ctx, { x, y })
                else if (block === BlockType.PRESERVED_TREE) drawPreservedTreeBlock(ctx, { x, y })
            })
        })

        // Draw bulldozer
        drawBulldozer(ctx, bulldozer.position, bulldozer.facing)
        // Draw crosshair
        drawCrosshair(ctx, bulldozer.position, bulldozer.facing, step)
    }
}
