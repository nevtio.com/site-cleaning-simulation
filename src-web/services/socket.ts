import EventEmitter from 'events'
import { io } from 'socket.io-client'
import { BlockType, Command, Expense, Facing, Position, ReportItem } from '../types'

const socket = io()
export const getSocket = () => {
    return socket
}
export const sendCommand = (command: string): Promise<true | number> => {
    return new Promise((resolve) => {
        socket.emit('command', { command }, (res: true | number) => {
            resolve(res)
        })
    })
}

export const getSiteData = (): Promise<{ blocks: BlockType[][]; bulldozer: { position: Position; facing: Facing } }> => {
    return new Promise((resolve) => {
        socket.emit('get-site-data', {}, (res: { blocks: BlockType[][]; bulldozer: { position: Position; facing: Facing } }) => {
            resolve(res)
        })
    })
}

export const listPerformedCommands = async (): Promise<Array<{ command: Command; expenses: Expense[] }>> => {
    return new Promise((resolve) => {
        socket.emit('list-performed-commands', {}, (performedCommands: Array<{ command: Command; expenses: Expense[] }>) => {
            resolve(performedCommands)
        })
    })
}

const em = new EventEmitter()
export const onSocketStatusChange = (listener: (status: string) => void): (() => void) => {
    em.addListener('status', listener)
    return () => {
        em.removeListener('status', listener)
    }
}
export const onSocketCommandExecuted = (listener: ({ command, expenses }: { command: Command; expenses: Expense[] }) => void): (() => void) => {
    em.addListener('command-executed', listener)
    return () => {
        em.removeListener('command-executed', listener)
    }
}
export const onSocketPerformedCommmandsUpdated = (listener: (performedCommands: Array<{ command: Command; expenses: Expense[] }>) => void): (() => void) => {
    em.addListener('performed-commands-updated', listener)
    return () => {
        em.removeListener('performed-commands-updated', listener)
    }
}
export const onSocketTerminate = (listener: (reportItems: ReportItem[]) => void): (() => void) => {
    em.addListener('terminate', listener)
    return () => {
        em.removeListener('terminate', listener)
    }
}

socket.on('connect', () => {
    em.emit('status', 'connected')
})
socket.on('disconnect', () => {
    em.emit('status', 'disconnected')
})
socket.on('connect_error', () => {
    em.emit('status', 'connection_error')
})
socket.on('command-executed', async ({ command, expenses }: { command: Command; expenses: Expense[] }) => {
    em.emit('command-executed', { command, expenses })
    const performedCommands = await listPerformedCommands()
    em.emit('performed-commands-updated', performedCommands)
})
socket.on('terminate', (reportItems: ReportItem[]) => {
    em.emit('terminate', reportItems)
})
