import React from 'react'
import { Command, Expense, CommandType } from '../types'

export interface LogProps {
    logs: Array<{ command: Command; expenses: Expense[] }>
}

const Log = (props: LogProps) => {
    return (
        <div className="log">
            <ul>
                {props.logs.map(({ command }: { command: Command; expenses: Expense[] }) => {
                    if (command.type === CommandType.LEFT) return <li>turn left</li>
                    else if (command.type === CommandType.RIGHT) return <li>turn right</li>
                    else if (command.type === CommandType.ADVANCE) return <li>advance {command.payload?.step}</li>
                })}
            </ul>
        </div>
    )
}

export default Log
