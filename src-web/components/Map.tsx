import React, { useCallback, useEffect, useRef } from 'react'
import { BlockType, Facing, Position } from '../types'
import { render } from '../services/renderer'
import debounce from 'debounce'

export interface MapProps {
    blocks: BlockType[][]
    bulldozer: {
        position: Position
        facing: Facing
    }
    step: number
}

const Map = (props: MapProps) => {
    const canvasEl = useRef(null)

    const refresh = useCallback(
        debounce((_props: MapProps) => {
            if (canvasEl.current) render(canvasEl.current as HTMLCanvasElement, _props.blocks, _props.bulldozer, _props.step)
        }, 100),
        [],
    )

    useEffect(() => {
        refresh(props)
    })

    return (
        <div className="map">
            <canvas ref={canvasEl}></canvas>
        </div>
    )
}

export default Map
