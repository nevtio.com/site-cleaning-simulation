import React from 'react'
import { Button, ButtonGroup } from 'react-bootstrap'
import { sendCommand } from '../services/socket'

export interface ControllerProps {
    step: number
    onStepChange: (step: number) => void
}

const Controller = (props: ControllerProps) => {
    const onTurnLeftClick = () => {
        sendCommand('l')
    }
    const onTurnRightClick = () => {
        sendCommand('r')
    }
    const onDecreaseStepClick = () => {
        if (props.step > 1) props.onStepChange(props.step - 1)
    }
    const onIncreaseStepClick = () => {
        props.onStepChange(props.step + 1)
    }
    const onAdvanceClick = () => {
        sendCommand(`a ${props.step}`)
    }

    return (
        <div className="controller d-flex justify-content-between">
            <div className="pb-3 text-center">
                <Button className="me-3" variant="primary" onClick={onTurnLeftClick} title="Turn Left">
                    &lt;
                </Button>
                <Button className="me-3" variant="primary" onClick={onTurnRightClick} title="Turn Right">
                    &gt;
                </Button>
            </div>
            <div className="pb-3 d-flex justify-content-center align-items-center">
                <Button variant="primary" onClick={onAdvanceClick}>
                    Advance
                </Button>
                <div className="px-3">{props.step}</div>
                <ButtonGroup aria-label="Basic example">
                    <Button variant="primary" onClick={onDecreaseStepClick}>
                        -
                    </Button>
                    <Button variant="primary" onClick={onIncreaseStepClick}>
                        +
                    </Button>
                </ButtonGroup>
            </div>
        </div>
    )
}

export default Controller
