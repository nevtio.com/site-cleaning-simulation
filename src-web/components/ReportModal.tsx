import React from 'react'
import { Modal, Table } from 'react-bootstrap'
import { ReportItem } from '../types'

export interface ReportModalProps {
    reportItems: ReportItem[]
}

const ReportModal = (props: ReportModalProps) => {
    const totalQty = props.reportItems.reduce((sum, reportItem) => sum + reportItem.qty, 0)
    const totalCost = props.reportItems.reduce((sum, reportItem) => sum + reportItem.cost, 0)
    return (
        <Modal show={props.reportItems.length > 0}>
            <Modal.Body>
                <div>The simulation has terminated. The costs for this land clearing operation were:</div>
                <br />
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                        {}
                        {props.reportItems.map((reportItem: ReportItem) => {
                            return (
                                <tr>
                                    <td>{reportItem.item}</td>
                                    <td>{reportItem.qty}</td>
                                    <td>{reportItem.cost}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Total</td>
                            <td>&nbsp;</td>
                            <td>{totalCost}</td>
                        </tr>
                    </tfoot>
                </Table>
            </Modal.Body>
        </Modal>
    )
}

export default ReportModal
