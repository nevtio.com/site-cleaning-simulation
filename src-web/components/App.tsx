import React, { useEffect, useState } from 'react'
import { Col, Container, Row, Spinner } from 'react-bootstrap'
import { getSiteData, listPerformedCommands, onSocketCommandExecuted, onSocketPerformedCommmandsUpdated, onSocketStatusChange, onSocketTerminate } from '../services/socket'
import { BlockType, Command, Expense, Facing, Position, ReportItem } from '../types'
import Controller from './Controller'
import Log from './Log'
import Map from './Map'
import ReportModal from './ReportModal'

const App = () => {
    // Data
    const [step, setStep] = useState(1)
    const [status, setStatus] = useState<string>('connecting')
    const [logs, setLogs] = useState<Array<{ command: Command; expenses: Expense[] }>>([])
    const [siteData, setSiteData] = useState<{ blocks: BlockType[][]; bulldozer: { position: Position; facing: Facing } }>({
        blocks: [],
        bulldozer: { position: { x: -1, y: -1 }, facing: Facing.EAST },
    })
    const [reportItems, setReportItems] = useState<ReportItem[]>([])

    // OnInit
    useEffect(() => {
        onSocketStatusChange((status: string) => {
            setStatus(status)
        })
        onSocketCommandExecuted(async ({ command }: { command: Command }) => {
            setSiteData(await getSiteData())
        })
        onSocketPerformedCommmandsUpdated((performedCommands: Array<{ command: Command; expenses: Expense[] }>) => {
            setLogs(performedCommands)
        })
        onSocketTerminate((reportItems: ReportItem[]) => {
            setReportItems(reportItems)
        })

        // Initialise logs
        setTimeout(async () => {
            setSiteData(await getSiteData())
            setLogs(await listPerformedCommands())
        }, 1)
    }, [])

    // Event handlers
    const onStepChange = (step: number) => {
        setStep(step)
    }

    return (
        <Container className="p-3">
            <Row>
                <Col>
                    <h1 className="heading">Site Cleaning Simulation</h1>
                    <p className="">Welcome to the Aconex site clearing simulator. This is a map of the site:</p>
                    {status === 'connecting' && (
                        <div>
                            <span className="pe-3">
                                <Spinner animation="border" size="sm" />
                            </span>
                            Connecting...
                        </div>
                    )}
                    {status === 'connected' && <div className="text-success">Connected</div>}
                    {status === 'disconnected' && reportItems.length === 0 && <div className="text-warning">Disconnected</div>}
                    {status === 'connection_error' && reportItems.length === 0 && <div className="text-danger">Error</div>}
                    {reportItems.length > 0 && <div className="text-info">Terminated</div>}
                </Col>
            </Row>
            <Row>
                <Col>
                    <Map blocks={siteData.blocks} bulldozer={siteData.bulldozer} step={step}></Map>
                </Col>
            </Row>
            <Row>
                <Col className="py-3">
                    <Controller step={step} onStepChange={onStepChange}></Controller>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Log logs={logs}></Log>
                </Col>
            </Row>

            <ReportModal reportItems={reportItems} />
        </Container>
    )
}
export default App
