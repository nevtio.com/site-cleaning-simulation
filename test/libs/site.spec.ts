import { Site } from '~/libs/site'
import { BlockType } from '~/types'

describe('libs/site', () => {
    it('Initialise Site instance', () => {
        // Default
        const site = new Site()
        expect(site).toBeDefined()
        expect(site).toBeInstanceOf(Site)
    })

    it('construct a new instnace with params', () => {
        // Default
        const site = new Site(3, 2, BlockType.PLAIN)
        expect(site.width).toBe(3)
        expect(site.height).toBe(2)
        expect(site.blocks).toEqual([
            ['o', 'o', 'o'],
            ['o', 'o', 'o'],
        ])
    })

    it('return correct value from load, clearBlock and getBlockType', () => {
        // Default
        const site = new Site()
        site.load('ootoo rrrTT')
        expect(site.width).toBe(5)
        expect(site.height).toBe(2)
        expect(site.getBlockType({ x: 1, y: 1 })).toBe(BlockType.ROCKY)
        expect(site.getBlockType({ x: 4, y: 1 })).toBe(BlockType.PRESERVED_TREE)
        expect(site.getBlockType({ x: 2, y: 0 })).toBe(BlockType.TREE)
        site.clearBlock({ x: 2, y: 0 })
        expect(site.getBlockType({ x: 2, y: 0 })).toBe(BlockType.PLAIN)
    })

    it('reset the site', () => {
        // Default
        const site = new Site(3, 2, BlockType.PLAIN)
        site.reset(BlockType.ROCKY, 2, 5)
        expect(site.width).toBe(2)
        expect(site.height).toBe(5)
        expect(site.getBlockType({ x: 0, y: 0 })).toBe(BlockType.ROCKY)
    })

    it('export site into string with toString', () => {
        // Default
        const site = new Site(3, 2, BlockType.PLAIN)
        expect(site.toString()).toBe('ooo ooo')
    })
})
