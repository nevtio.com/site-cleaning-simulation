module.exports = {
    globals: {
        __DEV__: true,
        'ts-jest': {
            tsConfig: './tsconfig.json',
            // babelConfig: "./.babelrc",
            hideStyleWarn: true,
        },
    },
    // setupFiles: ["jest-canvas-mock"],
    moduleFileExtensions: ['js', 'json', 'ts', 'tsx'],
    moduleNameMapper: {
        '^~/(.*)$': '<rootDir>/src/$1',
    },
    collectCoverageFrom: ['<rootDir>/src/**/*.ts'],
    transform: {
        // "^.+\\.js$": "babel-jest",
        '^.+\\.(ts|tsx)$': 'ts-jest',
    },
    transformIgnorePatterns: ['/node_modules/'],
}
